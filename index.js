const https = require('https');
const fs = require('fs');
const path = require('path');
const readline = require('readline');

const numeral = require('numeral');
const googleTTS = require('google-tts-api');
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
  {name: 'inputFile', alias: 'f', type: String},
  {name: 'outputDir', alias: 'd', type: String}
];

const options = commandLineArgs(optionDefinitions);

const rd = readline.createInterface({
  input: fs.createReadStream(options.inputFile),
  console: false
});

if (!fs.existsSync(options.outputDir)) {
  fs.mkdirSync(options.outputDir);
}

let index = 0;

rd.on('line', (line) => {
  const lineNumber = index;
  index++;
  googleTTS(line, 'en', 1)
      .then((url) => {
        const formatedNumber = numeral(lineNumber).format('0000');
        const fileName = `${formatedNumber}_${line}.mp3`;
        const filePath = path.join(options.outputDir, fileName);
        const file = fs.createWriteStream(filePath);
        https.get(url, (response) => {
          response.pipe(file);
        });
      }).catch((err) => {
    console.error(err.stack);
  });
});
